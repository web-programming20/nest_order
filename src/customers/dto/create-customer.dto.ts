import { IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(3, 50)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @IsPositive()
  @Length(10)
  tel: number;

  @IsNotEmpty()
  @Length(2)
  gender: string;
}
